﻿cron "process_current" do
  hour "5"
  minute "01"
  day "2-31"
  mailto "nick@snapterm.com"
  command "php /home/ec2-user/snap-data/agg-month.php current"
end
cron "process_previous" do
  hour "5"
  minute "01"
  day "1"
  mailto "nick@snapterm.com"
  command "php /home/ec2-user/snap-data/agg-month.php previous"
end
cron "restart_dynamic_dynamodb_service" do
  hour "6"
  minute "00"
  mailto "nick@snapterm.com"
  command "/usr/local/bin/dynamic-dynamodb --daemon restart -c /home/ec2-user/dynamic_dynamodb.conf"
end